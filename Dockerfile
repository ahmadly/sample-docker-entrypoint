FROM nginx:alpine

# this command run inside docker build time
RUN apk --no-cache add curl bash


ADD entrypoint.sh /usr/bin/entrypoint.sh

# this file run in docket run time
ENTRYPOINT ["/usr/bin/entrypoint.sh"]

# CMD will run after ENTRYPOINT and in docker run time
# also we cn overwrite cmd in hypervisour like kuber or compose
CMD ["echo","im main CMD"]
