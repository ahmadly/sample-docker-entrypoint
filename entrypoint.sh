#!/usr/bin/env bash
set -Eeo pipefail

# here you can run multiple commands in runtime

echo "manage.py migrate"
echo "manage.py makemessages"
echo "manage.py loaddata"
echo "manage.py collect static"

# now you can return CMD command
exec "$@"

